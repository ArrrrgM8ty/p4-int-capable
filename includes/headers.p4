header_type ethernet_t {
    fields {
        dstAddr : 48;
        srcAddr : 48;
        etherType : 16;
    }
}

header_type ipv4_t {
    fields {
        version : 4;
        ihl : 4;
        diffserv : 8;
        totalLen : 16;
        identification : 16;
        flags : 3;
        fragOffset : 13;
        ttl : 8;
        protocol : 8;
        hdrChecksum : 16;
        srcAddr : 32;
        dstAddr: 32;
    }
}

header_type udp_t{
    fields {
	srcPort: 16;
	dstPort: 16;
	len: 16;
	checksum: 16;
    }
}

header_type routing_metadata_t {
    fields {
        nhop_ipv4:	32;
    }
}

header_type l3_metadata_t {
    fields {
        lkp_l4_sport: 16;
	lkp_l4_dport: 16;
    }
}

header_type tunnel_metadata_t {
    fields {
        ingress_tunnel_type : 5;               /* tunnel type from parser */
        tunnel_vni : 24;                       /* tunnel id */
        mpls_enabled : 1;                      /* is mpls enabled on BD */
        mpls_label: 20;                        /* Mpls label */
        mpls_exp: 3;                           /* Mpls Traffic Class */
        mpls_ttl: 8;                           /* Mpls Ttl */
        egress_tunnel_type : 5;                /* type of tunnel */
        tunnel_index: 14;                      /* tunnel index */
        tunnel_src_index : 9;                  /* index to tunnel src ip */
        tunnel_smac_index : 9;                 /* index to tunnel src mac */
        tunnel_dst_index : 14;                 /* index to tunnel dst ip */
        tunnel_dmac_index : 14;                /* index to tunnel dst mac */
        vnid : 24;                             /* tunnel vnid */
        tunnel_terminate : 1;                  /* is tunnel being terminated? */
        tunnel_if_check : 1;                   /* tun terminate xor originate */
        egress_header_count: 4;                /* number of mpls header stack */
        inner_ip_proto : 8;                    /* Inner IP protocol */
        skip_encap_inner : 1;                  /* skip encap_process_inner */
    }
}

header_type int_metadata_t {
    fields {
        source :		2;
        sink :			2;
	padding :		12;
        gpe_int_hdr_len :	8;
        gpe_int_hdr_len8 :	8;
        switch_id : 		8;
        insert_cnt :		8;
        instruction_cnt :	8;
        insert_byte_cnt :	8;
    }
}
/***********************************************/
/*      VXLAN GPE header                       */
/***********************************************/

// header type length 64 bits = 8 bytes = 2 words
header_type vxlan_gpe_header_t {
    fields {
        flags		: 8;
	rsvd1		: 16;
	next_proto	: 8;
	vni		: 24;
	rsvd2		: 8;
    }
}


/***********************************************/
/* INT Shim Header for VXLAN GPE encapsulation */
/***********************************************/
/*
* Fields:
*********
* int_type - 8b
*   Possible values of the type field are HOP-BY-HOP or DESTINATION
*
* rsvd - 8b
*
* len - 8b
*   The len field holds the total length of the variable INT option data
*   (a.k.a. INT Metadata Headers and Metadata) and the Shim Header.
*   The value of this field is in 4B words units.
*
* next_proto - 8b
*   Possible values of next protocol field are:
*   0x01    IPv4
*   0x02    IPv6
*   0x03    Ethernet
*   0x04    Network Service Header (NSH)
*   0x05    In-band Network Telemetry Header (INT Header)
*/

// header type length 32 bits = 4 bytes = 1 word
header_type vxlan_gpe_int_header_t {
    fields {
        int_type    : 8;
        rsvd        : 8;
        len         : 8;
        next_proto  : 8;
    }
}

/************************************************/
/*                  INT Header                  */
/************************************************/
/*
* Fields:
*********
* ver - 2b
*   Metadata Header version. Should be zero for this version.
*
* rep - 2b
*   Replication requested. Use in case of multi-path forwarding techniques.
*   Possible values are:
*   0   No replication requested
*   1   Port-level (L2 level) replication requested
*   2   Next-hop-level (L3 level) replication requested
*   3   Port & Next hop level replication requested
*
* c - 1b
*   When replication is requested, the copy bit should be set for all copied
*   packets and reset for the original packet
*
* e - 1b
*   When Max Hop Count is reached this bit is set indicating that an INT
*   transit cannot push it's own INT data onto the stack
*
* rsvd1 - 5b
*
* ins_cnt - 5b
*   Instruction Count is set according to the number of instructions that
*   are set in the instruction bitmap
*
* max_hop_cnt - 8b
*   The maximum number of hops that are allowed to push metadata to the stack.
*   If this number is exceeded, the exceed bit is set and the next hop ignores
*   the instructions bitmap.
*
* total_hop_cnt - 8b
*   Total number of hops that pushed their metadata to the stack.
*
* instructions - 16b
*   The instructions each INT-transit should follow (e.g. what metadata to attach)
*   The instructions here are split for convinient lookup.
*   bit0 (MSB):     Switch ID
*   bit1:           Ingress Port ID
*   bit2:           Hop Latency
*   bit3:           Queue Occupancy
*   bit4:           Ingress Timestamp
*   bit5:           Egress Port ID
*   bit6:           Queue Congestion status
*   bit7:           Egress Port tx utilization
*   bit8 - bit15:   Reserved
*
* rsvd2 - 16b
*
*/
//header type length 64 bits = 8 bytes = 2 words
header_type int_header_t {
    fields {
        ver                     : 2;
        rep                     : 2;
        c                       : 1;
        e                       : 1;
        rsvd1                   : 5;
        ins_cnt                 : 5;
        max_hop_cnt             : 8;
        total_hop_cnt           : 8;
        instruction_mask_0003   : 4;  /* split the bits for lookup */
        instruction_mask_0407   : 4;
        instruction_mask_0811   : 4;
        instruction_mask_1215   : 4;
        rsvd2                   : 16;
    }
}

/************************************************/
/*           INT Metavalue Headers              */
/************************************************/
/* different header for each value type */
//header type length 32 bits = 4 bytes = 1 word
header_type int_switch_id_header_t {
    fields {
        bos                 : 1;
        switch_id           : 31;
    }
}

header_type int_ingress_port_id_header_t {
    fields {
        bos                 : 1;
        ingress_port_id     : 31;
    }
}

header_type int_hop_latency_header_t {
    fields {
        bos                 : 1;
        hop_latency         : 31;
    }
}

header_type int_q_occupancy_header_t {
    fields {
        bos                 : 1;
        q_occupancy         : 31;
    }
}

header_type int_ingress_tstamp_header_t {
    fields {
        bos                 : 1;
        ingress_tstamp      : 31;
    }
}

header_type int_egress_port_id_header_t {
    fields {
        bos                 : 1;
        egress_port_id      : 31;
    }
}

header_type int_q_congestion_header_t {
    fields {
        bos                 : 1;
        q_congestion        : 31;
    }
}

header_type int_egress_port_tx_utilization_header_t {
    fields {
        bos                         : 1;
        egress_port_tx_utilization  : 31;
    }
}
