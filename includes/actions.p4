action set_nhop(nhop_ipv4, port){
    modify_field(routing_metadata.nhop_ipv4, nhop_ipv4);
    modify_field(standard_metadata.egress_spec, port);
    add_to_field(ipv4.ttl, -1);
}

action set_dmac(dmac){
    modify_field(ethernet.dstAddr, dmac);
}

action rewrite_mac(smac){
    modify_field(ethernet.srcAddr, smac);
    /*add_header(test2);
    modify_field(test2.f1, -1);*/
}

action _drop(){
    drop();
}

action int_source() {
    /* add switch id header to metastack */
    // this header is of type int_switch_id_header_t and it's length is 4 bytes = 1 word
    add_header(int_switch_id_header);
    modify_field(int_switch_id_header.switch_id, 1);

    /* add int header */
    // this header is of type int_header_t and it's length is 8 bytes = 2 words
    add_header(int_header);
    modify_field(int_header.ver, 0);
    modify_field(int_header.c, 0);
    modify_field(int_header.e, 0);
    modify_field(int_header.rsvd1, 0);
    modify_field(int_header.ins_cnt, 1);
    modify_field(int_header.max_hop_cnt, 1);
    modify_field(int_header.total_hop_cnt, 1);
    modify_field(int_header.instruction_mask_0003, 8);
    modify_field(int_header.instruction_mask_0407, 0);
    modify_field(int_header.instruction_mask_0811, 0);
    modify_field(int_header.instruction_mask_1215, 0);
    modify_field(int_header.rsvd2, 0);

    /* add shim header */
    // this header is of type vxlan_gpe_int_header_t and it's length is 4 bytes = 1 word
    add_header(vxlan_gpe_int_header);
    modify_field(vxlan_gpe_int_header.int_type, 0);
    modify_field(vxlan_gpe_int_header.rsvd, 0);
    modify_field(vxlan_gpe_int_header.len, 4);
    modify_field(vxlan_gpe_int_header.next_proto, 0x05);

    /* add vxlan header */
    // this header is of type vxlan_gpe_header_t and it's length is 8 bytes = 2 words
    add_header(vxlan_gpe);
    modify_field(vxlan_gpe.flags, 0x04);
    modify_field(vxlan_gpe.rsvd1, 0x0000);
    modify_field(vxlan_gpe.next_proto, 0x05);
    modify_field(vxlan_gpe.vni, 0xaa55aa);
    modify_field(vxlan_gpe.rsvd2, 0x00);

    // we added 4 new headers with total length of 24 bytes = 6 words
    add_to_field(ipv4.ihl, 6);
    add_to_field(ipv4.totalLen, 24);
}
