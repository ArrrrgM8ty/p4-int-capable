table drop_expired {
    actions {
        _drop;
    }
}

table send_frame {
    reads {
        standard_metadata.egress_port: exact;
    }
    actions {
        rewrite_mac;
        _drop;
    }
    size: 256;
}

table ipv4_lpm {
    reads {
        ipv4.dstAddr : lpm;
    }
    actions {
        set_nhop;
        _drop;
    }
    size: 1024;
}

table forward {
    reads {
        routing_metadata.nhop_ipv4: exact;
    }
    actions {
        set_dmac;
        _drop;
    }
    size: 512;
}


/*************************************************************/

table int_insert {
    reads {
       int_metadata.source: exact;
    }
    actions {
        int_source;
    }
    size : 2;
}
