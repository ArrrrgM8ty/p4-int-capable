#define VXLAN_GPE_NEXT_PROTO_INT        0x0805 mask 0x08ff
#define UDP_PORT_VXLAN_GPE              4790
#define ETHERTYPE_IPV4                  0x0800
#define UDP_PROTO_NUM                   0x11
#define INGRESS_TUNNEL_TYPE_VXLAN_GPE	1


metadata routing_metadata_t 			routing_metadata;
metadata l3_metadata_t 				l3_metadata;
metadata tunnel_metadata_t 			tunnel_metadata;
metadata int_metadata_t 			int_metadata;

header ethernet_t 				ethernet;
header ipv4_t 					ipv4;
header udp_t 					udp;
header vxlan_gpe_header_t 			vxlan_gpe;
header vxlan_gpe_int_header_t           	vxlan_gpe_int_header;
header int_header_t                     	int_header;
header int_switch_id_header_t           	int_switch_id_header;
header int_ingress_port_id_header_t     	int_ingress_port_id_header;
header int_hop_latency_header_t         	int_hop_latency_header;
header int_q_occupancy_header_t         	int_q_occupancy_header;
header int_ingress_tstamp_header_t     		int_ingress_tstamp_header;
header int_egress_port_id_header_t      	int_egress_port_id_header;
header int_q_congestion_header_t        	int_q_congestion_header;
header int_egress_port_tx_utilization_header_t  int_egress_port_tx_utilization_header;


parser start {
    return parse_ethernet;
}

parser parse_ethernet {
    extract(ethernet);
    return select(latest.etherType){
        ETHERTYPE_IPV4 : parse_ipv4;
        default : ingress;
    }
}

field_list ipv4_checksum_list {
        ipv4.version;
        ipv4.ihl;
        ipv4.diffserv;
        ipv4.totalLen;
        ipv4.identification;
        ipv4.flags;
        ipv4.fragOffset;
        ipv4.ttl;
        ipv4.protocol;
        ipv4.srcAddr;
        ipv4.dstAddr;
}

field_list_calculation ipv4_checksum {
    input {
        ipv4_checksum_list;
    }
    algorithm : csum16;
    output_width : 16;
}

calculated_field ipv4.hdrChecksum  {
    verify ipv4_checksum;
    update ipv4_checksum;
}

parser parse_ipv4 {
    extract(ipv4);
    return select(latest.protocol){
        UDP_PROTO_NUM : parse_udp;
        default : parse_vxlan_gpe;
    }
}

parser parse_udp {
    extract(udp);
    set_metadata(l3_metadata.lkp_l4_sport, latest.srcPort);
    set_metadata(l3_metadata.lkp_l4_dport, latest.dstPort);
    return select(latest.dstPort) {
        UDP_PORT_VXLAN_GPE : parse_vxlan_gpe;
        default: ingress;
    }
}

parser parse_vxlan_gpe {
    extract(vxlan_gpe);
    set_metadata(tunnel_metadata.ingress_tunnel_type,
                 INGRESS_TUNNEL_TYPE_VXLAN_GPE);
    set_metadata(tunnel_metadata.tunnel_vni, latest.vni);
    return select (vxlan_gpe.flags, vxlan_gpe.next_proto) {
        VXLAN_GPE_NEXT_PROTO_INT : parse_gpe_int_header;
        // the parse_inner_ethernet parser is not implemented yet.
        //default : parse_inner_ethernet;
	default: parse_gpe_int_header;
    }
}


parser parse_gpe_int_header {
    extract(vxlan_gpe_int_header);
    set_metadata(int_metadata.gpe_int_hdr_len, latest.len);
    return parse_int_header;
}

parser parse_int_header {
    extract(int_header);
    set_metadata(int_metadata.instruction_cnt, latest.ins_cnt);
    return select (latest.rsvd1, latest.total_hop_cnt) {
        // reserved bits = 0 and total_hop_cnt == 0
        // no int_values are added by upstream
        0x000: ingress;
        // use an invalid value below so we never transition to
        // the state
        0x100: parse_all_int_meta_value_heders;
        default: ingress;
    }
}

parser parse_all_int_meta_value_heders {
    // bogus state.. just extract all posiible int headers in the
    // correct order to build the correct parse graph for deparser
    extract(int_switch_id_header);
    extract(int_ingress_port_id_header);
    extract(int_hop_latency_header);
    extract(int_q_occupancy_header);
    extract(int_ingress_tstamp_header);
    extract(int_egress_port_id_header);
    extract(int_q_congestion_header);
    extract(int_egress_port_tx_utilization_header);
    return ingress;
}
