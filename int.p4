/*******************************************************************/
/*                                                     control.p4                                                            */
/*******************************************************************/
#include "includes/headers.p4"
#include "includes/parsers.p4"
#include "includes/actions.p4"
#include "includes/tables.p4"

control ingress {
    if(valid(ipv4)) {
        if(ipv4.ttl > 1){
            apply(ipv4_lpm);
            apply(forward);
            apply(int_insert);
        } else {
            apply(drop_expired);
        }
    }
}

control egress {
    apply(send_frame);
}
