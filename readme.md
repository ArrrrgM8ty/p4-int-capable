# P4 int capable

this project implements int over vxlan on TCP IPv4 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

GO TO https://github.com/p4lang/behavioral-model

clone the project and install the dependencies  (USE THE travis source directory)

### Running
first make the json file:

	p4c-bm --json <path to target JSON file> <path to P4 file INT.p4>


change directory to /mininet under the behavioral-modal 

	sudo python 1sw_demo.py --behavioral-exe ../targets/simple_switch/simple_switch --json <path to Json File from previous stage>

mininet teminal should run with the  2 hosts and 1 switch    h1 -- s1 -- h2

change directory to targets/simple_switch under behavioral-modal


	./runtime_CLI < <path to command.txt under p4-int-capable>



## resources

http://p4.org/wp-content/uploads/fixed/INT/INT-current-spec.pdf

## Authors
Gal Berger,
Bhnir1


